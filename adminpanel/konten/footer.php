<div id="footer">
    <?php
    date_default_timezone_set('Asia/Jakarta');
    $tahun = date("Y");
    ?>
    &copy; <?php echo $tahun ?> <a href="http://schoolhos.com/" target="_blank">Schoolhos opensource CMS sekolah</a><br>
    Designed and developed by <a href="http://arirusmanto.com/" target="_blank">arirusmanto.com</a>. All rights reserved
</div>
</body>
</html>